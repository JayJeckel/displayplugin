<?php
/*
 * Display Plugin Dokuwiki Plugin
 * Copyright (c) 2016 Jay Jeckel
 * Licensed under the MIT license: https://opensource.org/licenses/MIT
 * Permission is granted to use, copy, modify, and distribute the work.
 * Full license information available in the project LICENSE file.
*/

namespace plugin\displayplugin;

final class Util
{
    private function __construct() { }

    public static function /* array */ asList(/* string */ $text, /* string */ $delimiter = ' ', /* int */ $limit = PHP_INT_MAX)
    { return empty($text) ? null : explode($delimiter, $text, $limit); }

    public static function onlyExisting(/* string */ $root, array $files = null, $sub = null, $ext = null)
    {
        $root = ltrim($root, '/') . '/';
        $subs = array();
        if (count($files) > 0) { foreach ($files as $file) { if (file_exists($root . $file)) { $subs[] = $file; } } }
        if (!empty($sub) && file_exists($root . $sub) && is_dir($root . $sub))
        {
            $subfiles = scandir($root . $sub);
            foreach ($subfiles as $subfile)
            {
                $path = $root . $sub . '/' . $subfile;
                if (!is_file($path)) { continue; }
                if (!empty($ext) && $ext != strtolower(pathinfo($path, PATHINFO_EXTENSION))) { continue; }
                $subs[] = $sub . '/' . $subfile;
            }
        }
        return $subs;
    }
}

final class Logic
{
    private function __construct() { }

    // There is a bug in GeShi, the syntax highlighting engine underlying Dokuwiki.
    // If it is passed content that starts with a < character, is told to highlight as
    // php, and contains a multiline comment of more than 610 characters, then the page
    // will crash. If any one of those isn't met, then the bug isn't raised.
    public static function isPhpFixCandidate($text, $language)
    { return $text[0] == '<' && ($language == 'php' || $language == 'php-brief'); }

    public static function isPhpFixRequired($text, $language)
    {
        if (Logic::isPhpFixCandidate($text, $language))
        {
            $result = preg_match_all('/\/\*.{610,}?\*\//s', $text);
            return $result === false || $result > 0;
        }
        return false;
    }





    // Returns error message or false; if false, then $content holds the file contents.
    public static function validate($root, $target, array $deny_extensions = null, array $allow_extensions = null, &$content)
    {
        $root = str_replace("\\", '/', $root);
        $root = rtrim($root, '/');

        // First, validate that the root path is set to something.
        if (!isset($root) || $root === '') { return 'error_root_empty'; }

        $real_root = realpath($root);

        // Second, check that the root path exists and is a directory.
        if ($real_root === false || !is_dir($real_root)) { return 'error_root_invalid'; }

        // We now know that root exists.

        // Third, defend against simple traversal attacks in the target.
        else if (strpos($target, '../') !== false) { return 'error_traversal_token'; }

        $ext = pathinfo($target , PATHINFO_EXTENSION);

        // Fourth, validate deny of extension.
        if (!empty($deny_extensions) && in_array($ext, $deny_extensions)) { return 'error_extension_deny'; }

        // Fifth, validate allow of extension.
        if (!empty($allow_extensions) && !in_array($ext, $allow_extensions)) { return 'error_extension_allow'; }

        $real_path = realpath($real_root . "/" . $target);

        // Sixth, catch the file trying to read itself.
        // Behavior undefined if this file doesn't exist.
        if (realpath(__FILE__) == $real_path) { return 'error_self'; }

        // !!ATTENTION!! Security must be considered in the following sections.
        //
        // The root path is assumed safe as it is provided by the admin,
        // but the target path stub is user-provided input and must be
        // treated as dangerous by default. Specifically, path traversal
        // attacks need to be mitigated.
        //
        // The main threat is use of path traversal shortcuts, such as the
        // '.', '..', and '~' tokens, to cause the resolved path to point at
        // files and directories outside the expected root path. Thankfully,
        // it is fairly easy to detect these attacks by normalizing and expanding
        // both the root and the path using realpath(). If the real path doesn't
        // literally begin with the real root, then you know you have a traversal.
        // Good, then that's done, right? Mostly, but not quite.
        //
        // The quirk is that realpath() only works for files that exist. If the
        // file doesn't exist, then you don't have a real path to compare to the
        // real root. Where the issue appears is when the developer follows their
        // natural instinct and tries to help the user by providing more useful
        // error messages; first check if the file doesn't exist, say as much; then
        // when we know the file exists, another message to say it is invalid if it
        // traverses outside the root.
        //
        // And there it is, you've opened a hole into the underlying system. What
        // we've done is give an attacker the ability to check if any arbitrary File
        // does or doesn't exist on whatever system is running the plugin. I know
        // that doesn't seem like much, but by checking the existence of files an
        // attacker can determine what OS the system is running, down to the specific
        // version of the OS, and that could inform more tailored attacks.
        //
        // Fortunately, there is a simple fix for this: deny the urge to be helpful.
        // Don't provide separate messages for the files that don't exist and files
        // that exist but have traversed outside the root path. This way the only
        // thing an attacker knows is that they can't access a file, nothing more, and
        // that is all they need to know. For good measure, all invalid paths should
        // return this same message to avoid any kind of proping of file information
        // through error messages.

        // Seventh, ensure the path exists.
        else if ($real_path === false) { return 'error_access'; }

        // We know that root and path exist.

        // Eighth, the final wall against path traversal outside the root.
        // Simple begins-with comparison, it's simple but effective.
        else if (strpos($real_path, $real_root) !== 0) { return 'error_access'; }

        // Ninth, ensure the file is a file and readable, and do any other good-measure checks.
        else if (is_dir($real_path)) { return 'error_access'; }
        else if (!is_readable($real_path)) { return 'error_access'; }

        // Tenth, attempt to read the contents and return error if failed.
        $result = file_get_contents($real_path);
        if ($result === false)
        {
            // This represents an actual error outside our control. Since the user should
            // have access to this content, it's ok to have a distinct error message.
            //return "The file '$title' could not be read.";
            return 'error_read';
        }

        // Finally, set the out variable to the result of the read and return null indicating no errors.
        $content = $result;
        return null;
    }
}

?>