# Display Plugin Dokuwiki Plugin

[![MIT License](https://svgshare.com/i/TRb.svg)](https://opensource.org/licenses/MIT)
[![DokuWiki Plugin](https://svgshare.com/i/TSa.svg)](https://www.dokuwiki.org/dokuwiki)
[![Plugin Home](https://svgshare.com/i/TRw.svg)](https://www.dokuwiki.org/plugin:displayplugin)
[![Gitlab Repo](https://svgshare.com/i/TRR.svg)](https://gitlab.com/JayJeckel/displayplugin)
[![Gitlab Issues](https://svgshare.com/i/TSw.svg)](https://gitlab.com/JayJeckel/displayplugin/issues)
[![Gitlab Download](https://svgshare.com/i/TT5.svg)](https://gitlab.com/JayJeckel/displayplugin/-/archive/master/displayplugin-master.zip)

The Display Plugin Plugin displays ?.

## Installation

Search and install the plugin using the [Extension Manager](https://www.dokuwiki.org/plugin:extension) or install directly using the latest [download url](https://gitlab.com/JayJeckel/displayplugin/-/archive/master/displayplugin-master.zip), otherwise refer to [Plugins](https://www.dokuwiki.org/plugins) on how to install plugins manually.

## Usage

The plugin offers a single block element that expands into the content of the specified plugin. The element is self-closing and should not be used as an open/close pair.

| Element | Note |
|:-|:-|
| `<<display plugin>>` ||

## Configuration Settings

The plugin provides several settings that can be modified through the [Configuration Manager](https://www.dokuwiki.org/config:manager).

| Setting | Default | Description |
|:-|:-|:-|
| `deny_plugins` | empty | Space-separated list of plugins that should not be displayed by the plugin. |

## Security

The plugin has no abnormal security concerns related to the provided functionality.
