<?php
/**
 * Display Plugin Dokuwiki Plugin (Syntax Component)
 *
 * Description: The Display Plugin Plugin ?
 *
 * Syntax: <<display plugin>>
 *
 * @license    The MIT License (https://opensource.org/licenses/MIT)
 * @author     Jay Jeckel <jeckelmail@gmail.com>
 *
 * Copyright (c) 2021 Jay Jeckel
 * Licensed under the MIT license: https://opensource.org/licenses/MIT
 * Permission is granted to use, copy, modify, and distribute the work.
 * Full license information available in the project LICENSE file.
 */

if (!defined('DOKU_INC')) { die(); }

require_once(dirname(__FILE__) . '/'. '_local.php');
use plugin\displayplugin\Util;
use plugin\displayplugin\Logic;

class syntax_plugin_displayplugin extends DokuWiki_Syntax_Plugin
{
    const PATTERN = '<<' . 'display' . '\s' . 'plugin' . '\s' . '[a-z0-9]+?' . '(?:\s(?:open|header|desc|source|close))?' . '>>';

    public function __construct()
    {
        $this->plugin = plugin_load('renderer', 'displayplugin_plugin');
    }

    public $plugin;

    function getInfo() { return confToHash(dirname(__FILE__) . '/plugin.info.txt'); }

    function getType() { return 'substition'; }

    function getPType() { return 'block'; }

    function getSort() { return 5; }

    function connectTo($mode)
    {
        $this->Lexer->addSpecialPattern(self::PATTERN, $mode, 'plugin_displayplugin');
    }

    function handle($match, $state, $pos, Doku_Handler $handler)
    {
        $match = substr($match, 17, -2);
        list($plugin, $part) = explode(' ', $match, 2);
        return array($plugin, $part);
    }

    function render($format, Doku_Renderer $renderer, $data)
    {
        if ($format == 'xhtml')
        {
            list($plugin, $part) = $data;
            $this->plugin->plugin($renderer, $plugin, $part, false);
            return true;
        }
        else if ($format == 'metadata')
        {
            list($plugin, $part) = $data;
            $this->plugin->plugin($renderer, $plugin, $part, true);
            return true;
        }
        return false;
    }
}

//Setup VIM: ex: et ts=4 enc=utf-8 :
?>