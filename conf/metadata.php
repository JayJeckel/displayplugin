<?php
/*
 * Display Plugin Dokuwiki Plugin
 * Copyright (c) 2021 Jay Jeckel
 * Licensed under the MIT license: https://opensource.org/licenses/MIT
 * Permission is granted to use, copy, modify, and distribute the work.
 * Full license information available in the project LICENSE file.
*/

$meta['deny_plugins'] = array('string');

?>