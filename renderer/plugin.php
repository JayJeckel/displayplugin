<?php
/*
 * Display File Plugin
 * Copyright (c) 2021 Jay Jeckel
 * Licensed under the MIT license: https://opensource.org/licenses/MIT
 * Permission is granted to use, copy, modify, and distribute the work.
 * Full license information available in the project LICENSE file.
 */

if (!defined('DOKU_INC')) { die(); }

require_once(dirname(__FILE__) . '/../'. '_local.php');
use plugin\displayfile\Util;
use plugin\displayfile\Logic;

class renderer_plugin_displayplugin_plugin extends DokuWiki_Plugin
{
    const ROOT = DOKU_INC . 'lib/plugins' . '/';

    const BY = 'by';
    const FROM = 'from';

    public function __construct()
    {
        $this->file = plugin_load('renderer', 'displayfile_file');
    }

    public $file;

    public $baselevel = 0;

    function plugin(Doku_Renderer $renderer, /* string */ $plugin, /* string */ $part, /* bool */ $meta = false)
    {
        $this->baselevel = $meta ? 0 : $renderer->getLastLevel();
        if (empty($part)) { }// Short circuit further comparing and drop down to rendering all parts.
        else if ($part == 'open') { $this->open($renderer, $meta); return; }
        else if ($part == 'header') { $this->header($renderer, $plugin, $meta); return; }
        else if ($part == 'desc') { $this->desc($renderer, $plugin, $meta); return; }
        else if ($part == 'source') { $this->source($renderer, $plugin, $meta); return; }
        else if ($part == 'close') { $this->close($renderer, $meta); return; }
        else { }

        $this->open($renderer, $meta);
        $this->header($renderer, $plugin, $meta);
        $this->desc($renderer, $plugin, $meta);
        $this->details($renderer, $plugin, $meta);
        $this->source($renderer, $plugin, $meta);
        $this->close($renderer, $meta);
    }

    function open(Doku_Renderer $renderer, /* bool */ $meta = false)
    { if (!$meta) { $renderer->doc .= '<div class="plugin__displayplugin">'. DOKU_LF; } }

    function close(Doku_Renderer $renderer, /* bool */ $meta = false)
    { if (!$meta) { $renderer->doc .= "</div>". DOKU_LF; } }

    function header(Doku_Renderer $renderer, /* string */ $plugin, /* bool */ $meta = false)
    {
        $dir = self::ROOT . $plugin . '/';
        $info = confToHash($dir . 'plugin.info.txt');
        if ($meta)
        {
            $this->_meta($renderer, $info['name'], 1);
            $renderer->meta['title'] = $info['name'];
        }
        else
        {
            $this->baselevel += 1;

            $renderer->doc .= '<div class="plugin__displayplugin_header">'. DOKU_LF;
            $renderer->header($info['name'], $this->baselevel, 0);
            $renderer->doc .= "</div>". DOKU_LF;

            $renderer->section_open($this->baselevel);
            $renderer->p_open();
            $renderer->cdata(' ');
            $renderer->doc .= '&#10149;';
            $renderer->cdata(' ');
            $renderer->externallink($info['url'], $info['base']);
            $renderer->cdata(' ');
            $renderer->superscript_open();
            $renderer->cdata($info['date']);
            $renderer->superscript_close();
            $renderer->cdata(' ' . self::BY . ' ');
            $renderer->emaillink($info['email'], $info['author']);

            if (!empty($info['sourcerepo']))
            {
                $renderer->cdata(' - ');
                $renderer->externallink($info['sourcerepo'], 'repo');
            }

            if (!empty($info['bugtracker']))
            {
                $renderer->cdata(' - ');
                $renderer->externallink($info['bugtracker'], 'issues');
            }

            if (!empty($info['downloadurl']))
            {
                $renderer->cdata(' - ');
                $renderer->externallink($info['downloadurl'], 'download');
            }

            if (!empty($info['donationurl']))
            {
                $renderer->cdata(' - ');
                $renderer->externallink($info['donationurl'], 'download');
            }

            $renderer->p_close();

            /*$renderer->p_open();
            $renderer->externallink('https://opensource.org/licenses/MIT',
                array('type' => 'externalmedia', 'src' => 'https://svgshare.com/i/TRb.svg'));
            $renderer->externallink('https://www.dokuwiki.org/dokuwiki',
                array('type' => 'externalmedia', 'src' => 'https://svgshare.com/i/TSa.svg'));
            $renderer->cdata(' ');
            $renderer->externallink($info['url'], array('type' => 'externalmedia', 'src' => 'https://svgshare.com/i/TRw.svg'));
            $renderer->cdata(' ');
            $renderer->externallink($info['sourcerepo'], array('type' => 'externalmedia', 'src' => 'https://svgshare.com/i/TRR.svg'));
            $renderer->cdata(' ');
            $renderer->externallink($info['bugtracker'], array('type' => 'externalmedia', 'src' => 'https://svgshare.com/i/TSw.svg'));
            $renderer->cdata(' ');
            $renderer->externallink($info['downloadurl'], array('type' => 'externalmedia', 'src' => 'https://svgshare.com/i/TT5.svg'));
            $renderer->p_close();
            $renderer->section_close();*/

            /*
            [[https://opensource.org/licenses/MIT|{{https://svgshare.com/i/TRb.svg}}]]
            [[https://www.dokuwiki.org/dokuwiki|{{https://svgshare.com/i/TSa.svg}}]]
            [[https://www.dokuwiki.org/plugin:displayfile|{{https://svgshare.com/i/TRw.svg}}]]
            [[https://gitlab.com/JayJeckel/displayfile|{{https://svgshare.com/i/TRR.svg}}]]
            [[https://gitlab.com/JayJeckel/displayfile/issues|{{https://svgshare.com/i/TSw.svg}}]]
            [[https://gitlab.com/JayJeckel/displayfile/repository/archive.zip?ref=master|{{https://svgshare.com/i/TT5.svg}}]]
             */
        }
    }

    function details(Doku_Renderer $renderer, /* string */ $plugin, /* bool */ $meta = false)
    {
        $dir = self::ROOT . $plugin . '/';
        $info = confToHash($dir . 'plugin.info.txt');
        if (!$meta)
        {
            $details = array();

            $list = Util::asList($info['depends'], ',');
            //if (!empty($list))
            { $details[] = array('Depends on', 'https://www.dokuwiki.org/plugin:%s', $list); }

            $list = Util::asList($info['conflicts'], ',');
            //if (!empty($list))
            { $details[] = array('Conflicts with', 'https://www.dokuwiki.org/plugin:%s', $list); }

            $list = Util::asList($info['similar'], ',');
            //if (!empty($list))
            { $details[] = array('Similar to', 'https://www.dokuwiki.org/plugin:%s', $list); }

            $list = Util::asList($info['tags'], ',');
            //if (!empty($list))
            { $details[] = array('Tagged with', 'https://www.dokuwiki.org/plugins?plugintag=%s#extension__table', $list); }

            $count = count($details);
            for ($index = 0; $index < $count; $index++)
            {
                $detail = $details[$index];

                if ($index == $count - 1) { $renderer->p_open(); }
                else { $renderer->doc .= DOKU_LF . '<p class="plugin__displayplugin_tight">' . DOKU_LF; }
                $renderer->strong_open();
                $renderer->cdata($detail[0]);
                $renderer->strong_close();
                $renderer->cdata(' ');
                $items = $detail[2];
                if (empty($items))
                {
                    $renderer->emphasis_open();
                    $renderer->cdata('NONE');
                    $renderer->emphasis_close();
                }
                else
                {
                    $rest = false;
                    foreach ($items as $item)
                    {
                        if ($rest) { $renderer->cdata(', '); } else { $rest = true; }
                        $item = trim($item);
                        $renderer->externallink(sprintf($detail[1], $item), $item);
                    }
                }
                $renderer->p_close();
            }
        }
    }

    function desc(Doku_Renderer $renderer, /* string */ $plugin, /* bool */ $meta = false)
    {
        if (!$meta)
        {
            $dir = self::ROOT . $plugin . '/';
            $info = confToHash($dir . 'plugin.info.txt');

            $renderer->p_open();
            $renderer->cdata($info['desc']);
            $renderer->p_close();
        }
    }

    function source(Doku_Renderer $renderer, /* string */ $plugin, /* bool */ $meta = false)
    {
        $dir = self::ROOT . $plugin . '/';

        $heading = 'Info Files';
        $files = Util::onlyExisting($dir, array('plugin.info.txt', 'LICENSE', 'LICENSE.md', 'README', 'README.md'), null, null);
        if (!$meta) { $this->_section($renderer, $plugin, $heading, $files, null); }
        else if (!empty($files)) { $this->_meta($renderer, $heading, 2); }

        $componentTypes = array('syntax', 'action', 'admin', 'helper', 'renderer', 'remote', 'auth', 'cli');
        $componentTitles = array('Syntax', 'Action', 'Administration', 'Helper', 'Renderer', 'Remote', 'Authentication', 'CLI');
        foreach ($componentTypes as $componentIndex => $componentType)
        {
            $heading = $componentTitles[$componentIndex] . ' Components';
            $files = Util::onlyExisting($dir, array($componentType . '.' . 'php'), $componentType, 'php');
            if (!$meta) { $this->_section($renderer, $plugin, $heading, $files, 'php'); }
            else if (!empty($files)) { $this->_meta($renderer, $heading, 2); }
        }

        $heading = 'CSS Files';
        $files = Util::onlyExisting($dir, array('style.css', 'print.css', 'all.css', 'feed.css'), null, null);
        if (!$meta) { $this->_section($renderer, $plugin, $heading, $files, 'css'); }
        else if (!empty($files)) { $this->_meta($renderer, $heading, 2); }

        $heading = 'JS Files';
        $files = Util::onlyExisting($dir, array('script.js'), 'scripts', 'js');
        if (!$meta) { $this->_section($renderer, $plugin, $heading, $files, 'javascript'); }
        else if (!empty($files)) { $this->_meta($renderer, $heading, 2); }

        $heading = 'Configuration Files';
        $files = Util::onlyExisting($dir, array('conf/default.php', 'conf/metadata.php'), null, null);
        if (!$meta) { $this->_section($renderer, $plugin, $heading, $files, 'php'); }
        else if (!empty($files)) { $this->_meta($renderer, $heading, 2); }

        $groups = array();
        if (file_exists($dir . 'lang') && is_dir($dir . 'lang'))
        {
            $langs = scandir($dir . 'lang');
            foreach ($langs as $lang)
            {
                if ($lang == '.' || $lang == '..') { continue; }
                $slug = 'lang' . '/' . $lang;
                if (is_dir($dir . $slug))
                {
                    $files = Util::onlyExisting($dir, array($slug . '/' . 'lang.php', $slug . '/' . 'settings.php'), $slug, 'txt');
                    if (count($files) > 0) { $groups[$lang] = $files; }
                }
            }
        }
        if (count($groups) > 0)
        {
            foreach ($groups as $lang => $files)
            {
                $heading = 'Language Files (' . $lang . ')';
                if (!$meta) { $this->_section($renderer, $plugin, $heading, $files, '?'); }
                else if (!empty($files)) { $this->_meta($renderer, $heading, 2); }
            }
        }
    }

    function _section(Doku_Renderer $renderer, $plugin, $section, array $files, $language = null, $options = null)
    {
        if (count($files) > 0)
        {
            $renderer->header($section, $this->baselevel + 1, 0);
            $renderer->section_open(2);
            foreach ($files as $file)
            {
                if ($language === '?') { $language = strtolower(pathinfo($file, PATHINFO_EXTENSION)); }
                $this->_file($renderer, $plugin, $file, $language, $options);
            }
            $renderer->section_close();
        }
    }

    function _file(Doku_Renderer $renderer, $plugin, $file, $language = null, $options = null)
    {
        if ($language === '?') { $language = strtolower(pathinfo($file, PATHINFO_EXTENSION)); }

        $root = DOKU_INC . 'lib/plugins' . '/' . $plugin;
        $content = 'CONTENT NOT SET';
        $error = Logic::validate($root, $file, null, null, $content);

        if (!empty($error))
        { $renderer->file("ERROR: {$error}", null, null); }
        else if (Logic::isPhpFixRequired($content, $language))
        { $this->file->_fixedfile($renderer, $file, $content, $language, $options); }
        else
        { $renderer->file($content, $language, $file, $options); }
    }

    function _meta(Doku_Renderer $renderer, /* string */ $text, /* bool */ $level)
    {
        if (!isset($renderer->meta['description']['tableofcontents']))
        { $renderer->meta['description']['tableofcontents'] = array(); }

        global $conf;
        if($level >= $conf['toptoclevel'] && $level <= $conf['maxtoclevel'])
        {
            $id = $renderer->_headerToLink($text);
            $renderer->meta['description']['tableofcontents'][] = html_mktocitem($id, $text, $level - $conf['toptoclevel'] + 1);
        }
    }
}

//Setup VIM: ex: et ts=4 enc=utf-8 :
?>